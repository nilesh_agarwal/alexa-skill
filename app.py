import logging
import os
from models import WorkoutTypes

from flask import Flask,json, render_template

from flask_ask import Ask, statement, question, session, audio, current_stream

app = Flask(__name__)

ask = Ask(app, "/")

logger = logging.getLogger()
logging.getLogger("flask_ask").setLevel(logging.DEBUG)

WARMUP_KEY = "WARMUP"
STRETCH_KEY = "STRETCH"
CARDIO_KEY = "CARDIO"
STRENGTH_KEY = "STRENGTH"

workout_countdown_url = "https://s3.amazonaws.com/alexa-skill-workout/workout-20.mp3"

@ask.launch
def new_workout():
    welcome_msg = render_template('welcome')
    return question(welcome_msg)

@ask.intent("WarmUpIntent")
def warmup_intent():
    workout = WorkoutTypes()

    # Get all the warmup exercises
    warmup_exercises = workout.get_all_exercises('Warmup')

    #retrieve the first 3 exercises as the limit is 90secs for audio
    warmup_exercises = warmup_exercises[:3]

    # get the audio url
    audiourl = render_template('audio')

    speech_output = ""
    for warmup_exercise in warmup_exercises:
        name = "Let's do " + warmup_exercise['Name']
        how_to_do = warmup_exercise['HowToDo']
        speech_output = speech_output + name + '\n' + '<break time="2s"/>' + how_to_do + '\n' + audiourl + '\n' + '<break time="3s"/>'

    workout_template = render_template('workout')
    speech_output = speech_output + workout_template

    exercise_msg = '<speak>{}</speak>'.format(speech_output)
    return question(exercise_msg)

@ask.intent("WarmUpNotIntent")
def warmup_not_intent():
    workout_msg = render_template('workout')
    reprompt_workout_msg = render_template('reprompt_workout')

    return question(workout_msg).reprompt(reprompt_workout_msg)

@ask.intent("WorkoutIntent",mapping={'workout': 'workouttype'})
def workout_intent(workout):

    workout_type = None
    if workout is not None:
        if workout == 'stretch' \
                or workout == 'strength' \
                or workout == 'warmup' \
                or workout == 'cardio':
            workout_type = workout
        else:
            return question(render_template('workout_error'))
    else:
        return question(render_template('workout_error'))

    workout = WorkoutTypes()

    # Get all the workout exercises
    workout_exercises = workout.get_all_exercises(workout_type)

    #retrieve the first 4 exercises as the limit is 90secs for audio
    workout_exercises = workout_exercises[:3]

    # get the audio url
    audiourl = render_template('audio')
    isStretch = False
    if workout_type=='stretch':
        isStretch = True

    speech_output = ""
    for workout_exercise in workout_exercises:
        name = "Let's do " + workout_exercise['Name']
        how_to_do = workout_exercise['HowToDo']

        if isStretch:
            tts = workout_exercise['TTS']
            speech_output = speech_output + name + '\n' + '<break time="2s"/>' + how_to_do + '\n' + tts + '\n' + '<break time="3s"/>'
        else:
            speech_output = speech_output + name + '\n' + '<break time="2s"/>' + how_to_do + '\n' + audiourl + '\n' + '<break time="3s"/>'

    workout_template = render_template('repeat_workout')
    speech_output = speech_output + workout_template

    exercise_msg = '<speak>{}</speak>'.format(speech_output)
    return question(exercise_msg)

@ask.intent('AMAZON.HelpIntent')
def help_intent():
    help_msg = render_template('help')
    return question(help_msg)

@ask.intent('AMAZON.CancelIntent')
def cancel():
    return audio('Sorry to see you go!Come back soon!').stop()

@ask.intent('AMAZON.PauseIntent')
def pause():
    return audio('Sorry to see you go!Come back soon!').stop()


@ask.intent('AMAZON.ResumeIntent')
def resume():
    return audio('Sorry to see you go!Come back soon!').resume()


@ask.intent('AMAZON.StopIntent')
def stop():
    return audio('Sorry to see you go!Come back soon!').clear_queue(stop=True)


# optional callbacks
@ask.on_playback_started()
def started(offset, token):
    _infodump('STARTED Audio Stream at {} ms'.format(offset))
    _infodump('Stream holds the token {}'.format(token))
    _infodump('STARTED Audio stream from {}'.format(current_stream.url))


@ask.on_playback_stopped()
def stopped(offset, token):
    _infodump('STOPPED Audio Stream at {} ms'.format(offset))
    _infodump('Stream holds the token {}'.format(token))
    _infodump('Stream stopped playing from {}'.format(current_stream.url))


@ask.on_playback_nearly_finished()
def nearly_finished():
    _infodump('Stream nearly finished from {}'.format(current_stream.url))


@ask.on_playback_finished()
def stream_finished(token):
    _infodump('Playback has finished for stream with token {}'.format(token))
    workout_msg = render_template('workout')
    #reprompt_workout_msg = render_template('reprompt_workout')

    return question(workout_msg)

@ask.session_ended
def session_ended():
    return "{}", 200


def _infodump(obj, indent=2):
    msg = json.dumps(obj, indent=indent)
    logger.info(msg)
    print(msg)

if __name__ == '__main__':
    if 'ASK_VERIFY_REQUESTS' in os.environ:
        verify = str(os.environ.get('ASK_VERIFY_REQUESTS', '')).lower()
        if verify == 'false':
            app.config['ASK_VERIFY_REQUESTS'] = False
    app.run(debug=True)

