import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')


class WorkoutTypes:

    def __init__(self):
        self.table = dynamodb.Table('WorkoutType')

    def insert(self, data):
        return self.table.put_item(Item=data)

    def get(self, name):
        response = self.table.get_item(
            Key={
                'Name': name
            }
        )
        return response['Item']

    def get_exercises(self, name, index):
        result = self.get(name=name)
        exercises = result['Exercises']
        exercise_id = exercises[index]
        exercise = Exercises()
        return exercise.get(exercise_id)

    def get_all_exercises(self, name):
        result = self.get(name=name)
        exercises_ids = result['Exercises']
        exercise = Exercises()
        exercises = []
        for exercises_id in exercises_ids:
            exercises.append(exercise.get(exercises_id))
        return exercises

    def delete(self, name):
        try:
            response = self.table.delete_item(
                Key={
                    'Name': name
                }
            )
        except ClientError as e:
            print("Could Not Delete Element")
            raise
        return response


class Exercises:

    def __init__(self):
        self.table = dynamodb.Table('Exercises')

    def insert(self, data):
        return self.table.put_item(Item=data)

    def get(self, exercise_id):
        response = self.table.get_item(
            Key={
                'ExerciseId': exercise_id
            }
        )
        return response['Item']

    def delete(self, exercise_id):
        try:
            response = self.table.delete_item(
                Key={
                    'ExerciseId': exercise_id
                }
            )
        except ClientError as e:
            print("Could Not Delete Element")
            raise
        return response



