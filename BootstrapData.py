# Get Workout Data

from models import WorkoutTypes, Exercises
import json


def insert_workouts():
    workout = WorkoutTypes()
    with open("data/Workout.json") as json_file:
        workoutTypes = json.load(json_file)
        for workoutType in workoutTypes['WorkoutTypes']:
            print(workoutType)
            workout.insert(workoutType)


def insert_exercises():
    exercises = Exercises()
    with open("data/Exercises.json") as json_file:
        exercises_data = json.load(json_file)
        for exercise in exercises_data['Exercises']:
            print(exercise)
            exercises.insert(exercise)


if __name__ == '__main__':
    insert_workouts()
    insert_exercises()